import React from "react";

export default function Funciones(prop){

    const miFuncion = () => {
        console.log("Hola");
    }


    return(
        
        <button onClick={ ()=> prop.saludarFn(prop.objAlumno.nombre) } >Saludar</button>
        // <button onClick={miFuncion} >Saludar</button>
    );
}


// cd existing_folder
// git init
// git remote add origin https://gitlab.com/sanchez77/cursodereact.git
// git add .
// git commit -m "Creando Rep, USO DE PROP para eniar parametros de padre a hijos"
// git push -u origin master



// git config --global user.name "jose"
// git config --global user.email "joseluissanchez41@hotmail.com"

// crear una rama en git --- git branch develop
// cambiar de rama --- git checkout develop
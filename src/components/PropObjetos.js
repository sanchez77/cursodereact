import React from "react";

export default function PropObjetos(props){

    // console.log(props.objAlumno.nombre);
    // console.log(props.objAlumno.edad);
    // console.log(props.objAlumno.nacio);
    return (
        <div>
            <h3>Pasando valores con Props usando Parametros</h3>
            <p>Hola {props.objAlumno.nombre} es un placer tenerlo de nuevo, 
            su edad actual es: {props.objAlumno.edad},
             usted nacio en : {props.objAlumno.nacio}.Saludos.
            </p>
        </div>
    );
}
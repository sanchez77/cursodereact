import logo from './logo.svg';
import './App.css';
import HolaMundo, {AdiosMundo} from "./components/HolaMundo";
import FinDelMundo from "./components/FinDelMundo";
import MiNombre from "./components/MiNombre"
import PropPractica from "./components/PropPractica";
import PropObjetos from "./components/PropObjetos"
import Funciones from "./components/Funciones";

function App() {


  const alumno = {
    nombre :  "Jose",
    edad : 24,
    nacio : "Guayaquil"
  };



  const saludarFn = name => {
    console.log("hola "+name+" desde app.js");
  }


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* < HolaMundo />
        < AdiosMundo />
        < FinDelMundo />
        < MiNombre /> */}
        {/* --------------------------- */}
        {/* Funciones */}
        <Funciones objAlumno={alumno} saludarFn = {saludarFn} />
        {/* --------------------------- */}
        {/* Pasando Objestos con Props */}
        {/* < PropObjetos name = "jose" objAlumno = {alumno}/> */}

        {/* -------------------------------- */}
        {/* Pansando variables con el uso de props */}
        {/* < PropPractica name="Jose Luis" edad="24" />

        < PropPractica name="Andrea Marquez" edad="15" /> */}
        {/* ------------------------------- */}

        {/* <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn joze
        </a> */}
      </header>
    </div>
  );
}

export default App;
